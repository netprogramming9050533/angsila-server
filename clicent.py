import socket
host = 'angsila.cs.buu.ac.th'
port = 22  

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect((host, port))

buffer = b""
while True:
    data = client_socket.recv(1024)
    if not data:
        break
    buffer += data

print(buffer.decode())
client_socket.close()
