import socket
host = 'angsila.cs.buu.ac.th'
port = 22  
filename = 'a.txt' 
  
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.connect((host, port))
print("Server listening...")

client_socket, client_address = server_socket.accept()
print("Connected to:", client_address)

with open('a.txt', 'r') as file:
    data = file.read(1024)
    while data:
        client_socket.send(data.encode())
        data = file.read(1024)

client_socket.close()
server_socket.close()
